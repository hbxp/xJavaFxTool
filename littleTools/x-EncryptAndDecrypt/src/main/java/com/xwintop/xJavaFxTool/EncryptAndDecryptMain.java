package com.xwintop.xJavaFxTool;

import javafx.application.Application;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EncryptAndDecryptMain {
    public static void main(String[] args) {
        Application.launch(EncryptAndDecryptApplication.class, args);
    }
}